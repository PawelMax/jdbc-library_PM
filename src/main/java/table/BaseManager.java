package table;

import api.Executor;
import config.Config;
import model.BaseModel;
import parser.DataParser;


public abstract class BaseManager  <T extends BaseModel> implements DataManager<T>, SqlQueries<T>{
    protected Executor  executor;
    protected Config config;
    protected DataParser<T> parser;

    public BaseManager(Executor executor, Config config, DataParser<T> parser) {
        this.executor = executor;
        this.config = config;
        this.parser = parser;
    }
}
