package table;

import api.ActionAdapter;
import api.Executor;
import config.Config;
import model.BaseModel;
import model.Book;
import parser.DataParser;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class BooksManager extends BaseManager<Book>{

    public BooksManager(Executor executor, Config config, DataParser<Book> dataParser) {
        super(executor, config, dataParser);
    }


    @Override
    public void createRepository() {
        executor.execute(new ActionAdapter() {
            @Override
            public void onExecute(Statement statement) {
                    try {
                        statement.executeUpdate(getCreateTableQuery());
                    }catch (SQLException e){
                        System.out.println(e.getMessage());
                    }
            }
        });

    }

    @Override
    public void add(final Book object) {
        executor.execute(new ActionAdapter() {
            @Override
            public void onExecute(Statement statement) {
                try {
                    statement.executeQuery(getInsertQuery(object));
                }catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
            }
        });

    }

    @Override
    public void update(Book object) {

    }

    @Override
    public List<Book> getList() {
        return null;
    }

    @Override
    public String getCreateTableQuery() {
        return null;
    }

    @Override
    public String getSelectQuery() {
        return null;
    }

    @Override
    public String getInsertQuery(Book object) {
        return null;
    }

    @Override
    public String getUpdateQuery() {
        return null;
    }
}
