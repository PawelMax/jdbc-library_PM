package config;

public class Config {
    private final String url;
    private final String user;
    private final String password;
    private final String dbName;

    public Config(String dbName, String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.dbName = dbName;
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getDbName() {
        return dbName;
    }
}
