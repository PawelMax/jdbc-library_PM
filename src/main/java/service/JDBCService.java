package service;

import java.sql.Connection;
/**
service used for base JSBC operations
 */

public interface JDBCService {

    boolean connect();
    void disconnect();
    Connection getConnection();
}
