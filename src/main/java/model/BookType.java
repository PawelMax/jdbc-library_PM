package model;

public enum BookType {
    PROSE("prose"),
    COMICS("comics"),
    FANTASY("fantasy"),
    POEM("poem"),
    DEFAULT("default");

    private String type;

    BookType(String type) {
        this.type = type;
    }

    public static BookType parseType(String type) {
        if (type.equals(PROSE.type)){
            return PROSE;
        } else if (type.equals(COMICS.type)) {
            return COMICS;
        } else if (type.equals(FANTASY.type)){
            return FANTASY;
        }else if (type.equals(POEM.type)){
            return POEM;
        }else {
            return DEFAULT;
        }
    }

    @Override
    public String toString() {
        return type;
    }
}
