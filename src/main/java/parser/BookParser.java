package parser;

import model.Author;
import model.Book;
import model.BookType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookParser implements DataParser<Book> {
    @Override
    public List<Book> parseToList(ResultSet resultSet) {
        List<Book> bookList = new ArrayList<>();
        try {
            while (resultSet.next()){
                String bookId = resultSet.getString(Book.BOOK_ID_COLUMN);
                String autorId = resultSet.getString(Book.AUTHOR_ID_COLUMN);
                String bookName = resultSet.getString(Book.BOOK_NAME_COLUMN);
                BookType bookType = BookType.parseType(resultSet.getString(Book.BOOK_TYPE_COLUMN));
                bookList.add(new Book(autorId, bookName, autorId, bookType));
            }
        }catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return bookList;
    }

}
