import api.DBExecutor;
import api.Executor;
import config.Config;
import model.Author;
import model.Book;
import model.BookType;
import parser.AuthorParser;
import parser.BookParser;
import parser.DataParser;
import service.JDBCService;
import service.MySQLService;
import table.AuthorsManager;
import table.BooksManager;
import table.DataManager;

import java.awt.*;
import java.sql.ResultSet;
import java.util.List;

public class Main {

    private static Menu menu = new Menu();

    public static void main(String[] args) {
        Config config = new Config("library", "jdbc:mysql://localhost/library", "root", "******");

        JDBCService service = new MySQLService(config);
        Executor executor = new DBExecutor(service);
        DataParser<Author> authorParser = new AuthorParser();
        DataParser<Book> bookParser = new BookParser();
        DataManager<Author> authorManager = new AuthorsManager(config, executor, authorParser);
        DataManager<Book> bookManager = new BooksManager(executor, config, bookParser);
        authorManager.createRepository();
        bookManager.createRepository();
        /*List<Author> authors  = authorManager.getList();
        for (Author author : authors) {
            System.out.println(author);
        }*/

        authorManager.add(new Author("3","Julian", "Tuwim", 1894));
        authorManager.add(new Author("1", "Czesław", "Miłosz", 1911));
        authorManager.add(new Author("2", "Krzysztof", "Wolski", 1993));
        authorManager.add(new Author("4","Stanisław", "Witkiewicz", 1885));
        authorManager.add(new Author("5","Adam", "Mickiewicz", 1798));
        authorManager.add(new Author("6","Henryk", "Sienkiewicz", 1846));
        authorManager.add(new Author("7","Wisława", "Szymborska", 1923));
        authorManager.add(new Author("8","Marek", "Hłasko", 1934));
        authorManager.add(new Author("9","Maria", "Dąbrowska", 1889));
        authorManager.add(new Author("10","Bruno", "Shultz", 1892));
        bookManager.add(new Book("0001", "Zniewolony Umysł", "1", BookType.PROSE));
        bookManager.add(new Book("0002", "Światło Dzienne", "1", BookType.POEM));
        bookManager.add(new Book("0003", "Dolina Issy", "1", BookType.PROSE));
        bookManager.add(new Book("0004", "Rzepka", "3", BookType.POEM));
        bookManager.add(new Book("0005", "Lokomotywa", "3", BookType.POEM));
        bookManager.add(new Book("0006", "Bambo", "3", BookType.POEM));








        //service.connect();
    }
}
