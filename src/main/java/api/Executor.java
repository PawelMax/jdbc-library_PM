package api;

import java.sql.ResultSet;

public interface Executor {
    void execute(Action action);

    ResultSet exeuteQuery(Action action);

    void execute(Action action, String sql);
}