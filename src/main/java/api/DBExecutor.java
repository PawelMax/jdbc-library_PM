package api;

import com.sun.rowset.CachedRowSetImpl;
import service.JDBCService;

import javax.sql.rowset.CachedRowSet;
import java.sql.*;
import java.util.Collections;

public class DBExecutor implements Executor {

    private JDBCService jdbcService;

    public DBExecutor(JDBCService jdbcService) {
        this.jdbcService = jdbcService;
    }

    @Override
    public void execute(Action action) {
        Statement statement = null;
        jdbcService.connect();
        Connection connection = jdbcService.getConnection();
        try {
            statement = connection.createStatement();
            action.onExecute(statement);//need to be implement
            System.out.println("create execution copleted!");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(statement);
        }
    }


    public ResultSet exeuteQuery(Action action) {
        ResultSet resultSet;
        Statement statement = null;
        jdbcService.connect();
        Connection connection = jdbcService.getConnection();
        try {
            statement = connection.createStatement();
            resultSet = action.onExecuteQuery(statement);
            CachedRowSet cachedRowSet = new CachedRowSetImpl();
            cachedRowSet.populate(resultSet);
            System.out.println("create execution copleted!!");
            return cachedRowSet;
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeConnection(statement);
        }
        return null;
    }

    @Override
    public void execute(Action action, String sql) {
        PreparedStatement statement = null;
        jdbcService.connect();
        Connection connection = jdbcService.getConnection();
        try {
            statement = connection.prepareStatement(sql);
            action.onExecute(statement);//need to be implement
            System.out.println("create execution copleted!");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(statement);
        }
    }

    private void closeConnection(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            }catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        jdbcService.disconnect();
    }
}
